import logging
from client import Client


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


client1 = Client('client1')
client2 = Client('client2')
client3 = Client('client3')
client4 = Client('client4')
client5 = Client('client5')
client6 = Client('client6')
client7 = Client('client7')
client8 = Client('client8')


client1.add_neighbors([client2, client3])
client2.add_neighbors([client1, client4])
client3.add_neighbors([client1, client4, client7])
client4.add_neighbors([client2, client3, client5])
client5.add_neighbors([client4, client6])
client6.add_neighbors([client5, client8, client7])
client7.add_neighbors([client6, client3])
client8.add_neighbors([client6])


client2.send_msg_to('client6', 'Some message!')
client6.send_msg_to('client2', 'Some message for you!')
client2.send_msg_to('client8', file_name='hello.txt')
client8.send_msg_to('client3', file_name='hello.txt')
client1.send_msg_to('client7', file_name='Практична робота №1.docx')
