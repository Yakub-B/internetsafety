import random
from utils import prime_generator, are_relatively_prime
from crypto.keys import PubKey, PrivKey


def get_p_q(size: int) -> tuple[int, int]:
    start = 1 << (size // 2 - 2)
    end = 1 << (size // 2 + 2)
    primes = list(prime_generator(start, end))

    p = random.choice(primes)
    lower_bound = 1 << (size - 1)
    upper_bound = (1 << size + 1) - 1
    q_list = [q for q in primes if lower_bound <= p * q <= upper_bound and q != p]
    q = random.choice(q_list)

    return p, q


def get_e(p: int, q: int) -> int:
    e_max = (p - 1) * (q - 1)
    for e in range(3, e_max):
        if are_relatively_prime(e, e_max):
            return e


def get_d(p: int, q: int, e: int) -> int:
    d_max = (p - 1) * (q - 1)
    for d in range(3, d_max):
        if d * e % d_max == 1:
            return d


def generate_key_pair(size: int = 8) -> tuple[PubKey, PrivKey]:
    p, q = get_p_q(size)
    e = get_e(p, q)
    d = get_d(p, q, e)
    n = p * q
    return PubKey(n, e), PrivKey(n, d)

