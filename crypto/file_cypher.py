import pathlib

from Crypto.Cipher import AES


class SymmetricFileCypher:
    @staticmethod
    def encrypt(key: bytes, file_path: pathlib.Path) -> pathlib.Path:
        if not file_path.exists() or not file_path.is_file():
            raise AssertionError(f'Bad input file!')

        cipher = AES.new(key, AES.MODE_EAX)
        with open(file_path, 'rb') as input_file:
            data = input_file.read()

        encoded, tag = cipher.encrypt_and_digest(data)

        output_file_path = file_path.parent / f'{file_path.name}.bin'
        with open(output_file_path, 'wb') as output_file:
            output_file.write(cipher.nonce)
            output_file.write(tag)
            output_file.write(encoded)

        return output_file_path

    @staticmethod
    def decrypt(key: bytes, file_path: pathlib.Path) -> pathlib.Path:
        if not file_path.exists() or not file_path.is_file():
            raise AssertionError(f'Bad input file!')

        with open(file_path, 'rb') as input_file:
            nonce = input_file.read(16)
            tag = input_file.read(16)
            encoded_data = input_file.read()

        cipher = AES.new(key, AES.MODE_EAX, nonce)
        decoded = cipher.decrypt_and_verify(encoded_data, tag)

        output_file_path = file_path.parent / file_path.stem
        with open(output_file_path, 'wb') as output_file:
            output_file.write(decoded)

        return output_file_path
