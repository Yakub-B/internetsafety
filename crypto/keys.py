class PubKey:
    _n: int
    _e: int

    def __init__(self, n: int, e: int):
        self._n = n
        self._e = e

    def encode(self, value: str) -> bytes:
        res = []
        for char in value:
            int_repr = pow(ord(char), self._e, self._n)
            res.append(str(int_repr))
        return ':'.join(res).encode('utf-8')
    
    def get_key(self) -> bytes:
        return f'{self._n}:{self._e}'.encode('utf-8')

    @staticmethod
    def from_bytes(key: bytes) -> 'PubKey':
        n, e = key.decode('utf-8').split(':')
        return PubKey(int(n), int(e))


class PrivKey:
    _n: int
    _d: int

    def __init__(self, n: int, d: int):
        self._n = n
        self._d = d

    def decode(self, value: bytes) -> str:
        value_string = value.decode('utf-8')
        res = ''

        for int_repr in value_string.split(':'):
            int_repr_decoded = pow(int(int_repr), self._d, self._n)
            res += chr(int_repr_decoded)
        return res
