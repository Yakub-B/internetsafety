import pathlib
import logging
from typing import Optional

from crypto.file_cypher import SymmetricFileCypher
from crypto.keys import PubKey, PrivKey
from crypto.rsa import generate_key_pair
from package import Package, File, MessageType
from utils import symmetric_password


class Client:
    _home_dir: pathlib.Path
    _pub_key: PubKey
    _priv_key: PrivKey
    address: str
    _pub_keys: dict[str, PubKey]
    _neighbors: dict[str, 'Client']
    _logger: logging.Logger
    _file_password: bytes

    def __init__(self, address: str):
        self.address = address
        self._pub_key, self._priv_key = generate_key_pair(20)

        self._home_dir = pathlib.Path('./home_dirs') / address
        if not self._home_dir.exists():
            self._home_dir.mkdir(parents=True)

        self._neighbors = {}
        self._pub_keys = {}
        self._logger = logging.getLogger(self.address)
        self._file_password = symmetric_password()

    def add_neighbors(self, neighbors: list['Client']):
        for c in neighbors:
            self._neighbors[c.address] = c
            self._pub_keys[c.address] = PubKey.from_bytes(c.get_pub_key())

    def get_pub_key(self) -> bytes:
        return self._pub_key.get_key()

    def send_msg_to(self, address: str, msg: str = None, file_name: str = None):
        if address not in self._pub_keys:
            self._logger.info(f'Does not know public key for {address=}. Requesting it...')
            response = self.propagate(
                Package(
                    from_=self.address,
                    to=address,
                    message=self._pub_key.get_key(),
                    message_type=MessageType.key_request,
                ),
                []
            )
            if response is None:
                raise LookupError(f'Client with {address=} not found!')

            self._pub_keys[address] = PubKey.from_bytes(response)
            self._logger.info(f'Got key from {address=}!')

        target_pub_key = self._pub_keys[address]
        if msg and file_name:
            raise AssertionError('Cannot send message and file at one time!')
        elif msg:
            pkg = Package(
                message=target_pub_key.encode(msg), message_type=MessageType.message
            )
        else:
            file_path = self._home_dir / file_name
            encrypted_file = SymmetricFileCypher.encrypt(self._file_password, file_path)

            with open(encrypted_file, 'rb') as ef:
                data = ef.read()

            pkg = Package(
                message=target_pub_key.encode(self._file_password.decode('utf-8')),
                file=File(name=encrypted_file.name, data=data),
                message_type=MessageType.file
            )

        pkg.from_ = self.address
        pkg.to = address
        response = self.propagate(pkg, [])

        if response is None:
            raise LookupError(f'Client with {address=} not found!')
        self._logger.info(f'Got response from {address=}: "{response.decode("utf-8")}"')

    def propagate(self, pkg: Package, visited: list[str]) -> Optional[bytes]:
        if target_client := self._neighbors.get(pkg.to):
            return target_client.receive_msg(pkg)

        visited.append(self.address)
        self._logger.info(f'Propagating message {pkg.message}')
        if pkg.message_type == MessageType.file:
            file = self._home_dir / pkg.file.name
            with open(file, 'wb') as f:
                f.write(pkg.file.data)
        for a, c in self._neighbors.items():
            if a in visited:
                continue
            response = c.propagate(pkg, visited)
            if response is not None:
                return response

    def receive_msg(self, pkg: Package) -> Optional[bytes]:
        self._logger.info(
            f'Got message from {pkg.from_}. Message type - {pkg.message_type}.'
        )
        match pkg.message_type:
            case MessageType.key_request:
                sender_pub_key = pkg.message
                self._pub_keys[pkg.from_] = PubKey.from_bytes(sender_pub_key)
                return self._pub_key.get_key()
            case MessageType.message:
                decoded_message = self._priv_key.decode(pkg.message)
                self._logger.info(f'Successfully decrypted message: "{decoded_message}"')
                return b'OK'
            case MessageType.file:
                file_password = self._priv_key.decode(pkg.message).encode('utf-8')
                input_file = self._home_dir / pkg.file.name
                with open(input_file, 'wb') as if_:
                    if_.write(pkg.file.data)

                output_file = SymmetricFileCypher.decrypt(file_password, input_file)
                self._logger.info(f'Successfully decrypted file: "{output_file}"')
                return b'OK'
        return None
