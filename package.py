from dataclasses import dataclass
from enum import Enum


@dataclass
class File:
    data: bytes
    name: str


class MessageType(str, Enum):
    key_request = 'KEY_REQUEST'
    file = 'FILE'
    message = 'MESSAGE'


@dataclass
class Package:
    message: bytes
    message_type: MessageType
    file: File = None
    from_: str = None
    to: str = None
