import random
import string


def symmetric_password(length: int = 16) -> bytes:
    chars = string.ascii_uppercase + string.digits
    return ''.join(random.choice(chars) for _ in range(length)).encode('utf-8')


def prime_generator(start,  end):
    for n in range(start, end + 1):
        for x in range(2, n):
            if n % x == 0:
                break
        else:
            yield n


def are_relatively_prime(a, b):
    for n in range(2, min(a, b) + 1):
        if a % n == 0 and b % n == 0:
            return False
    return True

